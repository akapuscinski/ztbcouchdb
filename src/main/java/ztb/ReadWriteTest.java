package ztb;


import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.DbPath;
import org.ektorp.http.HttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import java.util.logging.Logger;

/**
 * Created by Adrian on 17.01.2016.
 */
public class ReadWriteTest {

    private HttpClient httpClient;
    private CouchDbInstance dbInstance;
    private CouchDbConnector db;
    private int connectionsArray[] = {1, 2, 4, 8, 16, 32, 64};

    public ReadWriteTest(HttpClient httpClient) {
        this.httpClient = httpClient;
        this.dbInstance = new StdCouchDbInstance(httpClient);
        this.db = new StdCouchDbConnector(Config.DB_NAME, dbInstance);
    }

    public void performTest() {
        if(!dbInstance.checkIfDbExists(new DbPath(Config.DB_NAME))){
            throw new IllegalStateException("Database doesn't exist");
        } else if(db.getDbInfo().getDocCount()==0){
            throw new IllegalStateException("Can't perform read/write test on empty database");
        };

        final Logger logger = Util.getFileLogger("ReadWriteTest");

        for (int i=0; i<connectionsArray.length; i++) {
            final ReadTest readTest = new ReadTest(connectionsArray[i], httpClient);
            Runnable read = new Runnable() {
                public void run() {
                    readTest.performTest(logger);
                }
            };

            for (int k=0; k<connectionsArray.length; k++){
                WriteTest writeTest = new WriteTest(connectionsArray[k], httpClient);
                Thread readThread = new Thread(read);
                readThread.start();
                writeTest.performTest(true, logger);

                try {
                    readThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
