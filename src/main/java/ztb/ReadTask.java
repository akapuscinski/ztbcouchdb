package ztb;

import org.ektorp.CouchDbConnector;
import org.ektorp.ViewQuery;
import org.ektorp.impl.StdCouchDbConnector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by Adrian on 18.01.2016.
 */
public class ReadTask implements Callable<List<TaskResult>> {

    private CouchDbConnector db;
    private int startId, endId;
    private ViewQuery query;

    public ReadTask(StdCouchDbConnector db, int startId, int endId) {
        this.startId=startId;
        this.endId = endId;
        this.db = db;

        this.query = new ViewQuery().dbPath(Config.DB_NAME).includeDocs(true).designDocId("_design/view").viewName("ids");
    }

    public List<TaskResult> call() throws Exception {
        List<TaskResult> results = new ArrayList<TaskResult>();

        for (int i = startId; i <= endId; i++) {
            TaskResult result = new TaskResult();
            result.setStartTime(System.currentTimeMillis());
            query.key(i);
            db.queryView(query, TestData.class);
            result.setFinishTime(System.currentTimeMillis());
            results.add(result);
        }
        return results;
    }
}

//COUCHDB Map Function PATH view/ids
//function(doc) {
//    if(doc._id) {
//        emit(doc._id, null);
//    }
//}
