package ztb;

import org.ektorp.support.CouchDbDocument;

/**
 * Created by Adrian on 16.01.2016.
 */
public class TestData extends CouchDbDocument {

    private String id;
    private String revision;
    private long timestamp;
    private String uri;
    private long l1, l2, l3, l4;
    private double d1, d2, d3, d4;
    private String text;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public long getL1() {
        return l1;
    }

    public void setL1(long l1) {
        this.l1 = l1;
    }

    public long getL2() {
        return l2;
    }

    public void setL2(long l2) {
        this.l2 = l2;
    }

    public long getL3() {
        return l3;
    }

    public void setL3(long l3) {
        this.l3 = l3;
    }

    public long getL4() {
        return l4;
    }

    public void setL4(long l4) {
        this.l4 = l4;
    }

    public double getD1() {
        return d1;
    }

    public void setD1(double d1) {
        this.d1 = d1;
    }

    public double getD2() {
        return d2;
    }

    public void setD2(double d2) {
        this.d2 = d2;
    }

    public double getD3() {
        return d3;
    }

    public void setD3(double d3) {
        this.d3 = d3;
    }

    public double getD4() {
        return d4;
    }

    public void setD4(double d4) {
        this.d4 = d4;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
