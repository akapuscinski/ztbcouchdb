package ztb;

import org.ektorp.BulkDeleteDocument;
import org.ektorp.CouchDbConnector;
import org.ektorp.http.HttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

/**
 * Created by Adrian on 16.01.2016.
 */
public class WriteTest {

    private int connections;
    private HttpClient httpClient;

    public WriteTest(int connections, HttpClient httpClient) {
        this.connections = connections;
        this.httpClient = httpClient;
    }

    public void performTest(boolean deleteAddedRecords, Logger logger) {
        logger.info("Connections: " + connections + " Records to insert: " + Config.RECORD_COUNT + "\n");
        long testStartMillis = System.currentTimeMillis();
        logger.info("Test starts at: " + Util.formatLongToTimeString(testStartMillis));

        ExecutorService pool = Executors.newFixedThreadPool(connections);
        List<TaskResult> results = new ArrayList<TaskResult>();
        List<Future<List<TaskResult>>> resultList = new ArrayList<Future<List<TaskResult>>>();

        //setup data repo
        StdCouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
        StdCouchDbConnector connector = new StdCouchDbConnector(Config.DB_NAME, dbInstance);
        DataRepo repo = new DataRepo(connector);

        int dataStep = Config.RECORD_COUNT / connections; //TODO would not work if RECORD_COUNT can't be divided by 64
        for (int i = 0; i < connections; i++) {
            resultList.add(pool.submit(new WriteTask(repo, dataStep)));
        }

        for (int i = 0; i < resultList.size(); i++) {
            try {
                results.addAll(resultList.get(i).get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        long testEndMillis = System.currentTimeMillis();
        logger.info("Test ended at: " + Util.formatLongToTimeString(testEndMillis));
        long totalTimeMillis = testEndMillis - testStartMillis;
        logger.info("Test total time in seconds: " + (double) totalTimeMillis/1000);
        logger.info("Records per second: " + (double) Config.RECORD_COUNT/(totalTimeMillis/1000));

        logger.info("Average write time millis: " + getAverage(results));
        logger.info("Median write time millis: " + getMedian(results));
        logger.info("Shortest write time millis: " + results.get(0).getTime());//everything is sorted after getMedian
        logger.info("Longest write time millis: " + results.get(results.size() - 1).getTime() + "\n");

        //for experiment 3
        if(deleteAddedRecords){
            List<TestData> toBeDeletedList = new ArrayList<TestData>();
            for (TaskResult result : results) {
                toBeDeletedList.add(result.getData());
            }
            deleteAddedDocs(toBeDeletedList, httpClient);
        }

        pool.shutdown();
    }

    private void deleteAddedDocs(List<TestData> data, HttpClient httpClient){
        StdCouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
        CouchDbConnector db = new StdCouchDbConnector(Config.DB_NAME, dbInstance);

        List<Object> bulk = new ArrayList<Object>();
        for (TestData testData : data) {
            bulk.add(BulkDeleteDocument.of(testData));
        }

        db.executeBulk(bulk);

    }

    private double getMedian(List<TaskResult> results) {
        Collections.sort(results);
        int middle = results.size() / 2;
        if (results.size() % 2 == 1) {
            return results.get(middle).getTime();
        } else {
            return (results.get(middle - 1).getTime() + results.get(middle).getTime()) / 2.0;
        }
    }

    private double getAverage(List<TaskResult> results) {
        long sum = 0;
        for (TaskResult r : results) {
            sum += r.getTime();
        }
        return sum / results.size();
    }

}
