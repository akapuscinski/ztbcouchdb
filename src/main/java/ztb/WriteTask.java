package ztb;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by Adrian on 16.01.2016.
 */
public class WriteTask implements Callable<List<TaskResult>> {

    private DataProvider provider;
    private int dataCount;
    private DataRepo repo;

    public WriteTask(DataRepo repo, int dataCount) {
        this.repo = repo;
        this.dataCount = dataCount;

        this.provider = new DataProvider();
    }

    public List<TaskResult> call() throws Exception {
        List<TaskResult> results = new ArrayList<TaskResult>();

        for (int i = 0; i < dataCount; i++) {
            TaskResult result = new TaskResult();
            result.setStartTime(System.currentTimeMillis());
            TestData data = provider.provideTestData();
            repo.add(data);
            result.setFinishTime(System.currentTimeMillis());
            result.setData(data);
            results.add(result);
        }
        return results;
    }
}
