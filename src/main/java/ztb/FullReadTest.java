package ztb;


import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.DbPath;
import org.ektorp.http.HttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import java.util.logging.Logger;

/**
 * Created by Adrian on 17.01.2016.
 */
public class FullReadTest {

    private HttpClient httpClient;
    private CouchDbInstance dbInstance;
    private CouchDbConnector db;
    private int connectionsArray[] = {1,2,4,8,16,32,64};

    public FullReadTest(HttpClient httpClient) {
        this.httpClient = httpClient;
        this.dbInstance = new StdCouchDbInstance(httpClient);
        this.db = new StdCouchDbConnector(Config.DB_NAME, dbInstance);
    }

    public void performFullTest(){
        if(!dbInstance.checkIfDbExists(new DbPath(Config.DB_NAME))){
            throw new IllegalStateException("Database doesn't exist");
        } else if(db.getDbInfo().getDocCount()==0){
            throw new IllegalStateException("Can't perform read test on empty database");
        };

        Logger logger = Util.getFileLogger("FullReadTest");

        for (int connections : connectionsArray) {
            DataProvider.clearCounter();
            ReadTest test = new ReadTest(connections, httpClient);
            test.performTest(logger);
        }
    }
}
