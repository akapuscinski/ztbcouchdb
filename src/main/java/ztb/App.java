package ztb;

import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;

import java.net.MalformedURLException;

public class App {

    public static void main(String[] args) throws MalformedURLException {

        /*
        [[[[[[[ARGUMENTS]]]]]]]]]]]]]]
        1 - single write test, REQUIRED int (connections number) OPTIONAL int (records count)
        2 - reserved for single read test NOT IMPLEMENTED YET
        3 - reserved for single readwrite test NOT IMPLEMENTED YET
        4 - full write test, clears db data before each next write test
        5 - full write test on not empty db, clears only new data added after single write test
        6 - full read test, db can't be empty, NEEDS MAP/REDUCE function in db
        7 - full read/write test, db can't be empty, clears new data added after single write test NEEDS MAP/REDUCE function in db
         */

        int type = Integer.parseInt(args[0]); //1 - WO, 2-RO, 3-RW, 4 - FULL WRITE TEST
        int connections = 0;

        if (type == -1) { //TESTING FUNCTIONS
            return;
        }

        if (type == 1 || type == 3) {
            connections = Integer.parseInt(args[1]);
        }

        if (args.length > 2) {
            Config.RECORD_COUNT = Integer.parseInt(args[2]);
        }

        HttpClient httpClient = new StdHttpClient.Builder()
                .url(Config.DB_ADDRESS)
                .maxConnections(64) //default is twenty
                .build();

        FullWriteTest fullWriteTest = new FullWriteTest(httpClient);

        if (type == 1) { //single write test, DO ONLY ON EMPTY DB
            WriteTest test = new WriteTest(connections, httpClient);
            test.performTest(false, Util.getFileLogger("SingleWriteTest"));
        } else if (type == 2) {
            //TODO single read
        } else if (type == 4) { //full write test on empty db
            fullWriteTest.performFullTest(false, true);
        } else if (type == 5) {
            DataProvider.setIdCounter(Config.RECORD_COUNT + 1); //can't duplicate ids
            fullWriteTest.performFullTest(true, false); //full write test on NOT empty db, after insertion new records are deleted
        } else if (type == 6) { //full read test
            FullReadTest fullReadTest = new FullReadTest(httpClient);
            fullReadTest.performFullTest();
        } else if (type == 7) {
            DataProvider.setIdCounter(Config.RECORD_COUNT + 1);

            Config.RECORD_COUNT = 3200; //set this test for less records due to record test time, there are 49
            // read/write combinations total

            ReadWriteTest test = new ReadWriteTest(httpClient);
            test.performTest();
        }
    }
}
