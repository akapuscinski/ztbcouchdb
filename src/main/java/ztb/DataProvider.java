package ztb;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Adrian on 16.01.2016.
 */
public class DataProvider {

    public static final int TEXT_SIZE = 32*1024; //in bytes
    public static final int URI_SIZE = 2*1024;
    public static final String INDEX_PREFIX = "beng";
    public static AtomicInteger idCounter = new AtomicInteger();
    
    private String textData;
    private String uriData;

    public DataProvider(){
        this.textData = makeStringWithSpecifiedLength('a', TEXT_SIZE);
        this.uriData = makeStringWithSpecifiedLength('b', URI_SIZE);
    }

    public TestData provideTestData(){
        TestData data = new TestData();
        data.setTimestamp(System.currentTimeMillis());
        data.setId(INDEX_PREFIX + idCounter.getAndIncrement());
        data.setText(this.textData);
        data.setUri(this.uriData);
        data.setL1(5000);
        data.setL2(6000);
        data.setL3(7000);
        data.setL4(8000);
        data.setD1(10.20);
        data.setD1(10.20);
        data.setD3(10.20);
        data.setD4(10.20);
        return data;
    }

    public static void setIdCounter(int number){DataProvider.idCounter.set(number);}

    public static void clearCounter(){
        DataProvider.idCounter.set(0);
    }

    private String makeStringWithSpecifiedLength(char charToFill, int length){
        if (length > 0) {
            char[] array = new char[length];
            Arrays.fill(array, charToFill);
            return new String(array);
        }
        return "";
    }
}
