package ztb;

/**
 * Created by Adrian on 16.01.2016.
 */
public class TaskResult implements Comparable<TaskResult> {

    private long startTime;
    private long finishTime;
    private TestData data;

    public TaskResult() {
    }

    public TaskResult(long startTime, long finishTime) {
        this.startTime = startTime;
        this.finishTime = finishTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    public long getTime(){
        return this.finishTime-this.startTime;
    }

    public TestData getData() {
        return data;
    }

    public void setData(TestData data) {
        this.data = data;
    }

    //sorts from shortest time to longest time
    public int compareTo(TaskResult o) {
        return (int) (this.getTime()-o.getTime());
    }
}
