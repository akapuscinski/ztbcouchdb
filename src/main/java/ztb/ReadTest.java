package ztb;

import org.ektorp.http.HttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

/**
 * Created by Adrian on 16.01.2016.
 */
public class ReadTest {

    private int connections;
    private HttpClient httpClient;

    public ReadTest(int connections, HttpClient httpClient) {
        this.connections = connections;
        this.httpClient = httpClient;
    }

    public void performTest(Logger logger) {
        logger.info("Connections: " + connections + " Records to read: " + Config.RECORD_COUNT + "\n");
        long testStartMillis = System.currentTimeMillis();
        logger.info("Test starts at: " + Util.formatLongToTimeString(testStartMillis));

        ExecutorService pool = Executors.newFixedThreadPool(connections);
        List<TaskResult> results = new ArrayList<TaskResult>();
        List<Future<List<TaskResult>>> resultList = new ArrayList<Future<List<TaskResult>>>();

        //setup db
        StdCouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
        StdCouchDbConnector connector = new StdCouchDbConnector(Config.DB_NAME, dbInstance);

        int dataStep = Config.RECORD_COUNT / connections; //TODO would not work if RECORD_COUNT can't be divided by 64
        for (int i = 0; i < connections; i+=dataStep) {
            resultList.add(pool.submit(new ReadTask(connector, i, ((i+1)*dataStep)-1)));
        }

        for (int i = 0; i < resultList.size(); i++) {
            try {
                results.addAll(resultList.get(i).get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        long testEndMillis = System.currentTimeMillis();
        logger.info("Test ended at: " + Util.formatLongToTimeString(testEndMillis));
        long totalTimeMillis = testEndMillis - testStartMillis;
        logger.info("Test total time in millis: " + totalTimeMillis);
        logger.info("Records retrieved per second: " + Config.RECORD_COUNT/((double) totalTimeMillis/1000));

        logger.info("Average read time millis: " + getAverage(results));
        logger.info("Median read time millis: " + getMedian(results));
        logger.info("Shortest read time millis: " + results.get(0).getTime());//everything is sorted after getMedian
        logger.info("Longest read time millis: " + results.get(results.size() - 1).getTime()+ "\n");

        pool.shutdown();
    }

    private double getMedian(List<TaskResult> results) {
        Collections.sort(results);
        int middle = results.size() / 2;
        if (results.size() % 2 == 1) {
            return results.get(middle).getTime();
        } else {
            return (results.get(middle - 1).getTime() + results.get(middle).getTime()) / 2.0;
        }
    }

    private double getAverage(List<TaskResult> results) {
        long sum = 0;
        for (TaskResult r : results) {
            sum += r.getTime();
        }
        return sum / results.size();
    }

}
