package ztb;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Adrian on 17.01.2016.
 */
public class Util {

    public static Logger getFileLogger(String filePrefix){
        Logger logger = Logger.getLogger(WriteTest.class.getSimpleName());
        FileHandler fh=null;
        try {
            fh = new FileHandler(filePrefix + "_" +  Util.getTimeForLogFile() + ".log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return logger;
    }

    public static String formatLongToTimeString(long timeInMillis){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss:SSS");
        Date date = new Date(timeInMillis);
        return format.format(date);
    }

    public static String getTimeAsString(){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss:SSS");
        Date date = new Date(System.currentTimeMillis());
        return format.format(date);
    }

    public static String getTimeForLogFile(){
        SimpleDateFormat format = new SimpleDateFormat("HH_mm");
        Date date = new Date(System.currentTimeMillis());
        return format.format(date);
    }
}
