package ztb;

import org.ektorp.CouchDbConnector;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.GenerateView;

import java.util.List;

/**
 * Created by Adrian on 16.01.2016.
 */
public class DataRepo extends CouchDbRepositorySupport<TestData> {

    protected DataRepo(CouchDbConnector db) {
        super(TestData.class, db);
    }

    @GenerateView
    public List<TestData> findById(String id){
        return queryView("ids", id);
    }

    public List<TestData> findByL1(int l1){
        return queryView("by_l1", l1);
    }
}
