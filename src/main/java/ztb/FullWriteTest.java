package ztb;


import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.DocumentNotFoundException;
import org.ektorp.http.HttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import java.util.logging.Logger;

/**
 * Created by Adrian on 17.01.2016.
 */
public class FullWriteTest {

    private HttpClient httpClient;
    private CouchDbInstance dbInstance;
    private CouchDbConnector db;
    private int connectionsArray[] = {1, 2, 4, 8, 16, 32, 64};

    public FullWriteTest(HttpClient httpClient) {
        this.httpClient = httpClient;
        this.dbInstance = new StdCouchDbInstance(httpClient);
        this.db = new StdCouchDbConnector(Config.DB_NAME, dbInstance);
    }

    public void performFullTest(boolean deleteAddedRecords, boolean clearBeforeAdding) {
        Logger logger = Util.getFileLogger("FullWriteTest");

        for (int connections : connectionsArray) {

            if (!deleteAddedRecords)
                DataProvider.clearCounter();

            if (clearBeforeAdding) {
                try {
                    this.dbInstance.deleteDatabase(Config.DB_NAME);
                } catch (DocumentNotFoundException e) {
                    //do nothing db not created earlier
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            this.db.createDatabaseIfNotExists();

            WriteTest test = new WriteTest(connections, httpClient);
            test.performTest(deleteAddedRecords, logger);
        }
    }
}
